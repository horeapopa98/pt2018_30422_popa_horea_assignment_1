package PT2018_30422_Popa_Horea_Assignment_1;

import javax.swing.*;

import Controller.Controlerul;
import Model.Monomial;
import Model.Polynom;
import View.GUI;

public class App
{
	public static void main( String[] args )
	{
		GUI grafUser = new GUI();
		grafUser.GUI(); 						//create an object and call the GUI method so the user interface can be displayed

		Controlerul controlerr = new Controlerul(grafUser);			//acces the methods from Controlerul with the object created before, grafUser
	}
}
