package Controller;

import java.awt.event.*;

import Model.Monomial;
import Model.Polynom;
import View.GUI;

public class Controlerul {

	Polynom poly1 = new Polynom();
	Polynom poly2 = new Polynom();

	GUI grapher = new GUI();

	public Controlerul(GUI guiUser) {
		this.grapher = guiUser;

		grapher.addAddMonomListener1(new AddMonomListener1());				//this method is the link between the ActionListeners and the user interface
		grapher.addAddMonomListener2(new AddMonomListener2());				//one ActionListener for every operation
		grapher.addAdditionPoly(new AdditionPoly());
		grapher.addSubstractionPoly(new SubstractionPoly());
		grapher.addMultiplicationPoly(new MultiplicationPoly());
		grapher.addDivisionPoly(new DivisionPoly());
		grapher.addDerivationPoly(new DerivationPoly());
		grapher.addIntegrationPoly(new IntegrationPoly());
		grapher.addClearPoly1(new ClearPoly1());
		grapher.addClearPoly2(new ClearPoly2());
	}


	class AddMonomListener1 implements ActionListener{						//use this method to write the monomials in the textFields
																			//use the split funtion to select the coefficient and the degree
		public void actionPerformed(ActionEvent arg0) {						//s[0] will always represent the coefficient
			// TODO Auto-generated method stub								//s[1] will always represent the degree
			String gt = "";													//the final form of the polynomial will be shown in the JLabel
			String[] s;														//method for the first polynomial
			gt = grapher.pol1TextField.getText();

			s=gt.split("x\\^");

			Monomial mon = new Monomial(Float.parseFloat(s[0]), Integer.parseInt(s[1]));
			poly1.addMonomial(mon);

			grapher.resPol1Label.setText(poly1.toString());
		}
	}


	class AddMonomListener2 implements ActionListener{						//use this method to write the monomials in the textFields
																			//use the split funtion to select the coefficient and the degree
		public void actionPerformed(ActionEvent arg1) {						//s[0] will always represent the coefficient
			// TODO Auto-generated method stub								//s[1] will always represent the degree
			String st = "";													//the final form of the polynomial will be shown in the JLabel
			String[] s;														//method for the second polynomial
			st = grapher.pol2TextField.getText();

			s = st.split("x\\^");

			Monomial mon2 = new Monomial(Float.parseFloat(s[0]), Integer.parseInt(s[1]));
			poly2.addMonomial(mon2);

			grapher.resPol2Label.setText(poly2.toString());
		}
	}


	class AdditionPoly implements ActionListener{							//call the addition method between poly1 and poly2 and store it in the result polynomial

		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			Polynom result = poly1.addition(poly2);
			grapher.resultTextField.setText(result.toString());
		}
	}


	class SubstractionPoly implements ActionListener{						//call the substraction method between poly1 and poly2 and store it in the result polynomial

		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			Polynom result = poly1.substraction(poly2);
			grapher.resultTextField.setText(result.toString());
		}
	}


	class MultiplicationPoly implements ActionListener{						//call the multiplication method between poly1 and poly2 and store it in the result polynomial

		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			Polynom result = poly1.multiplication(poly2);
			grapher.resultTextField.setText(result.toString());
		}
	}


	class DivisionPoly implements ActionListener{							//call the division method between poly1 and poly2 and store it in the result polynomial
																			//result[0] will be the quotient
		public void actionPerformed(ActionEvent e) {						//result[1] will be the remainder
			// TODO Auto-generated method stub
			Polynom[] result = poly1.division(poly2);
			grapher.resultTextField.setText("quotient: "+result[0].toString()+"\n"+"remainder: "+result[1].toString());
		}
	}


	class DerivationPoly implements ActionListener{							//call the derivation method for poly1, derivation will be stored in result
																			//call the derivation method for poly2, derivation will be stored in resul2
		public void actionPerformed(ActionEvent e) {						//both will appear in resultTextField
			// TODO Auto-generated method stub
			Polynom result = poly1.derivation(poly1);
			Polynom result2 = poly2.derivation(poly2);
			grapher.resultTextField.setText(result.toString()+"\n"+result2.toString());
		}
	}


	class IntegrationPoly implements ActionListener{						//call the integration method for poly1, integration will be stored in result
																			//call the integration method for poly2, integration will be stored in resul2
		public void actionPerformed(ActionEvent e) {						//both will appear in resultTextField
			// TODO Auto-generated method stub
			Polynom result = poly1.integration(poly1);
			Polynom result2 = poly2.integration(poly2);
			grapher.resultTextField.setText(result.toString()+"\n"+result2.toString());
		}
	}

	class ClearPoly1 implements ActionListener{								//clear the first polynomial and the result

		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub;
			poly1.equation.clear();
			grapher.pol1TextField.setText(" ");
			grapher.resPol1Label.setText(" ");
			grapher.resultTextField.setText(" ");
		}
	}

	class ClearPoly2 implements ActionListener{								//clear the second polynomial and the result

		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			poly2.equation.clear();
			grapher.pol2TextField.setText(" ");
			grapher.resPol2Label.setText(" ");
			grapher.resultTextField.setText(" ");
		}
	}

}

