package View;

import java.awt.event.*;
import javax.swing.*;
import java.io.*;


public class GUI {

	public JFrame frame;																//here I made the GUI, I declared all the buttons "public" because I use them in another
	public JTextField pol1TextField = new JTextField();									//classes, in the GUI() method I set the sizes and the locations for the
	public JTextField pol2TextField = new JTextField();									//buttons, textFields and labels
	public JLabel resPol1Label = new JLabel("Polynomial 1");
	public JLabel resPol2Label = new JLabel("Polynomial 2");
	public JButton pol1Button = new JButton("ADD");
	public JButton pol2Button = new JButton("ADD");
	public JButton addButton = new JButton(" +  ADD ");
	public JButton subButton = new JButton(" -  SUBTRACT ");
	public JButton multButton = new JButton(" *  MULTIPLY ");
	public JButton divButton = new JButton(" /  DIVIDE ");
	public JButton derivButton = new JButton(" DERIVATION ");
	public JButton integButton = new JButton(" INTEGRATION ");
	public JTextArea resultTextField = new JTextArea(2,0);		//I am using a JTextArea because I need 2 rows for division, and for derivation and integration
	public JButton clear1Button = new JButton("CLEAR 1");		//the program can execute the operations for both polynomials, I use this TextArea to show the finale result
	public JButton clear2Button = new JButton("CLEAR 2");		//no matter the operation

	public void GUI() {

		frame = new JFrame();
		frame.setLayout(null);
		frame.setSize(900,900);
		frame.setTitle("Polynomial Operations");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		pol1TextField.setSize(260,50);
		pol1TextField.setLocation(50,80);
		frame.add(pol1TextField);

		pol2TextField.setSize(260,50);
		pol2TextField.setLocation(50,180);
		frame.add(pol2TextField);

		JLabel pol1Label = new JLabel("Add to polynomial 1");
		pol1Label.setSize(150,70);
		pol1Label.setLocation(50,35);
		frame.add(pol1Label);

		JLabel pol2Label = new JLabel("Add to polynomial 2");
		pol2Label.setSize(150,70);
		pol2Label.setLocation(50,135);
		frame.add(pol2Label);

		pol1Button.setSize(80,40);
		pol1Button.setLocation(310,85);
		frame.add(pol1Button);

		pol2Button.setSize(80,40);
		pol2Button.setLocation(310,185);
		frame.add(pol2Button);

		resPol1Label.setSize(400,70);
		resPol1Label.setLocation(520,70);
		frame.add(resPol1Label);

		resPol2Label.setSize(400,70);
		resPol2Label.setLocation(520,170);
		frame.add(resPol2Label);
		
		JLabel taskLabel = new JLabel("Insert polynomials as shown here:  cx^d");
		taskLabel.setSize(500,120);
		taskLabel.setLocation(50,-30);
		frame.add(taskLabel);

		JLabel opLabel = new JLabel("Select which operation you want to perform with the 2 polynomials:");
		opLabel.setSize(500,120);
		opLabel.setLocation(50,210);
		frame.add(opLabel);

		addButton.setSize(180,65);
		addButton.setLocation(140,300);
		frame.add(addButton);

		subButton.setSize(180,65);
		subButton.setLocation(340,300);
		frame.add(subButton);

		multButton.setSize(180,65);
		multButton.setLocation(140,380);
		frame.add(multButton);

		divButton.setSize(180,65);
		divButton.setLocation(340,380);
		frame.add(divButton);

		derivButton.setSize(180,65);
		derivButton.setLocation(140,460);
		frame.add(derivButton);

		integButton.setSize(180,65);
		integButton.setLocation(340,460);
		frame.add(integButton);

		JLabel resultLabel = new JLabel("RESULT:");
		resultLabel.setSize(100,120);
		resultLabel.setLocation(310,550);
		frame.add(resultLabel);

		resultTextField.setSize(260,50);
		resultTextField.setLocation(210,620);
		resultTextField.setEditable(false);
		frame.add(resultTextField);

		clear1Button.setSize(110,40);
		clear1Button.setLocation(400,85);
		frame.add(clear1Button);

		clear2Button.setSize(110,40);
		clear2Button.setLocation(400,185);
		frame.add(clear2Button);

		frame.setVisible(true);
	}

	public void addAddMonomListener1 (ActionListener addml1) {							//for every operation I associate a button, the algorythms for all operations
		pol1Button.addActionListener(addml1);											//are implemented in the Polynom class and in the class Controlerul
	}																					//I have all the ActionListeners to read the data and execute the operations
	//when the associated button is pressed
	public void addAddMonomListener2 (ActionListener addml2) {
		pol2Button.addActionListener(addml2);
	}

	public void addAdditionPoly(ActionListener addpoly) {
		addButton.addActionListener(addpoly);
	}

	public void addSubstractionPoly(ActionListener subpoly) {
		subButton.addActionListener(subpoly);
	}

	public void addMultiplicationPoly(ActionListener multipoly) {
		multButton.addActionListener(multipoly);
	}

	public void addDivisionPoly(ActionListener divpoly) {
		divButton.addActionListener(divpoly);
	}

	public void addDerivationPoly(ActionListener derivpoly) {
		derivButton.addActionListener(derivpoly);
	}

	public void addIntegrationPoly(ActionListener integrpoly) {
		integButton.addActionListener(integrpoly);
	}

	public void addClearPoly1(ActionListener clear1) {									//clear the first polynom and the result to put different values while the
		clear1Button.addActionListener(clear1);											//program is still running
	}

	public void addClearPoly2(ActionListener clear2) {									//clear the second polynom and the result to put different values while the
		clear2Button.addActionListener(clear2);											//program is still running
	}


}
