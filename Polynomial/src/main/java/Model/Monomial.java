package Model;

public class Monomial {

	private int degree = 0;										//declare the parameters for every monomial and initialise them
	private float coefficient = 0;								//float will be used for derivation, integration and division

	public Monomial(float coefficient, int degree) {
		this.coefficient = coefficient;
		this.degree = degree;
	}

	public int getDegree() {
		return degree;
	}

	public void setDegree(int degree) {
		this.degree = degree;
	}

	public float getCoefficient() {
		return coefficient;
	}

	public void setCoefficient() {
		this.coefficient = coefficient;
	}
}