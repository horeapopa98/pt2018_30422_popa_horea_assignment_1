package Model;

import java.util.*;

public class Polynom {

	public List <Monomial> equation = new ArrayList<Monomial>(); 								//represents a list of monomials, aka the polynom


	public Polynom() {
		equation = new ArrayList<Monomial>();
	}


	public void cleanUpAndSort() {
																						//cleanUp
		List <Monomial> removedMonomials = new ArrayList<Monomial>();					//this part of the method represents the "clean up", in some operations will remain in
																						//the polynom monomials with coeff 0, so I create a list with those mons. If the coeff
		for (Monomial mon1:equation) {													//is higher than -0.0001 and lower than 0.0001 I consider it to have the coeff 0 and
			if (mon1.getCoefficient() >= -0.0001 && mon1.getCoefficient() <= 0.0001)	//will be put in the removedMonomials list that will then be cleaned up
				removedMonomials.add(mon1);
		}
		equation.removeAll(removedMonomials);

		Collections.sort(equation, new Comparator <Monomial>() {						//here I am sorting the monomials in decreasing order by the degree. I made a compare
																						//function that will return 3 values, -1 if negative, 0 if equal, 1 if positive
			public int compare(Monomial arg0, Monomial arg1) {
				// TODO Auto-generated method stub
				return arg1.getDegree()-arg0.getDegree();
			}
		});
	}


	public void addMonomial(Monomial mon) {
																						//method for adding a monomial to the polynom
		equation.add(mon);
		cleanUpAndSort();
	}


	public int getDegree() {
																						//implemented method to get the degree of the polynomial, because it is sorted,
		cleanUpAndSort();																//it will take the degree of the first mon in the list and that will be the
		if(equation.size()==0)															//degree of the poly, if there are no mons it will return -1
			return -1;
		return equation.get(0).getDegree();
	}


	public String toString() {															//method that puts conditions on how the polynomials will be written in the
		cleanUpAndSort();																//textField, the for function will go through all the polynom and see where
																						//the "+" sign will be put, also if the coefficient will be negative, no + sign
		if(equation.size()==0)															//will be put before him, the negative values will be written in the textField
			return "0";																	//in front of the coeff

		String polynom = "";

		for(int i=0;i<equation.size();i++) {
			Monomial mon1=equation.get(i);
			if (mon1.getCoefficient()!=0) {
				if (i>0 && mon1.getCoefficient()>0)
					polynom+="+";
				polynom+=mon1.getCoefficient() + "x^" + mon1.getDegree();
			}
		}
		return polynom;
	}


	public boolean verifyExistMon(int degree, List <Monomial> result) {					//method that verifies if a monomial exists in the result of the op between the 2 poly
		for(Monomial mon:result) {
			if (mon.getDegree() == degree) {
				return false;
			}
		}
		return true;
	}


	public Polynom addition(Polynom p1) {												//ADDITION
		Polynom result = new Polynom();													//first of all, I search for the monomials with the same degree in both polynomials
																						//to be added in the result. Add only the coefficients because the degree is the same
		for(Monomial mon1:this.equation) {
			for(Monomial mon2:p1.equation) {
				if (mon1.getDegree() == mon2.getDegree()) {
					Monomial newMon = new Monomial(mon2.getCoefficient() + mon1.getCoefficient(),mon1.getDegree());
					result.addMonomial(newMon);
				}
			}
		}

		for(Monomial mon1:this.equation) {												//second of all, I search for the monomials which do not have the same degree with any
			if (verifyExistMon(mon1.getDegree(),p1.equation)) {							//monomial from the second polynom. Those monomials will be added in the result.
				result.addMonomial(mon1);												//I do this for both polynomials
			}																			//this.equation--->poly1
		}																				//p1.equation----->poly2

		for(Monomial mon1:p1.equation) {
			if (verifyExistMon(mon1.getDegree(),this.equation)) {
				result.addMonomial(mon1);
			}
		}

		return result;
	}

	public Polynom substraction(Polynom p1) {											//SUBSTRACTION
		Polynom result = new Polynom();													//same algorithm as the one from Addition, the only difference is that here
																						//I substract everytime from the monomial from the first polynom, I can also put the
		for(Monomial mon1:this.equation) {												//degree from any monomial if they are equal
			for(Monomial mon2:p1.equation) {											//monomials will be added in the result polynomial
				if (mon1.getDegree() == mon2.getDegree()) {
					Monomial newMon = new Monomial(mon1.getCoefficient() - mon2.getCoefficient(),mon1.getDegree());
					result.addMonomial(newMon);
				}
			}
		}

		for(Monomial mon1:this.equation) {												//search for the monomials that do not have equal degrees and add them in the result
			if (verifyExistMon(mon1.getDegree(),p1.equation)) {							//hence this is the first polynomial, the sign of the coefficient will not be affected
				result.addMonomial(mon1);												//in the result
			}
		}

		for(Monomial mon1:p1.equation) {												//search for the monomials in the second polynomial, if it does not find any monomials
			if (verifyExistMon(mon1.getDegree(),this.equation)) {						//with equal degrees from the first polynomial, add them to the result polynomial
				result.addMonomial(new Monomial(-mon1.getCoefficient(),mon1.getDegree()));//hence this is the second polynomial and some coefficients may be negative,
			}																			//(-)(-) = + so the coefficient of a monomial from the second polynomial will always
		}																				//have the negative sign in front of him

		return result;
	}

	public Polynom multiplication(Polynom p1) {											//MULTIPLICATION
		Polynom result = new Polynom();													//the algorithm for this is the following:take the first monomial from the first polynom
																						//make a new polynom named partialResult, multiply the first monomial from the first poly
		for(Monomial mon1:this.equation) {												//with every monomial from the second polynomial, add the result to the partialResult poly.
			Polynom partialResult = new Polynom();										//Then the result poly will take the value from the old result plus the partialResult poly.
			for(Monomial mon2:p1.equation) {											//Do this for every monomial from the first poly. The final polynom will be stored in result poly.
				partialResult.addMonomial(new Monomial(mon1.getCoefficient()*mon2.getCoefficient(), mon1.getDegree()+mon2.getDegree()));
			}
			result=result.addition(partialResult);
		}
		return result;
	}

	public Polynom[] division(Polynom p1) {					//DIVISION
		Polynom remainder = new Polynom();					//quotien will be the result of the division and remainder will be the "rest"
		remainder.equation.addAll(equation);				//the remainder is actually the first poly which will be divided.
															//for the division to take place the degree of the remainder must be higher than the degree of the 2nd poly. The method will
		Polynom quotient = new Polynom();					//use some previous methods, like substraction and multiplication. The point of this method is that we must take the monomiale
															//from the 1st poly with the highest degree and divide it with the monomial with the highest degree from the second poly.
		while(remainder.getDegree()>=p1.getDegree())		//The result will be stored in the quotient. Then, the new monomial from the quotient will be multiplied with every monomial
		{													//from the 2nd poly and then stored in a new poly named prod with a negated sign of the coefficient.The new remainder is going to be the substraction
			Monomial q1 = new Monomial(remainder.equation.get(0).getCoefficient()/p1.equation.get(0).getCoefficient(), remainder.equation.get(0).getDegree()-p1.equation.get(0).getDegree());
			quotient.addMonomial(q1);						//of the last remainder and the prod. This algorithm goes on until the monomial with the highest degree from the 1st poly
			Polynom prod = new Polynom();					//will have a lower degree then the monomial with the highest degree from the second poly.
			prod.addMonomial(q1);
			prod=prod.multiplication(p1);
			remainder=remainder.substraction(prod);
			remainder.cleanUpAndSort();
		}

		Polynom[] result = new Polynom[2];
		result[0]=quotient;
		result[1]=remainder;
		return result;
	}

	public Polynom derivation(Polynom p1) {												//DERIVATION
		Polynom result = new Polynom();													//We have the monomial cx^d. The formula for derivation is:(c*d)x^(d-1).
																						//Apply this for every monomial.
		for(Monomial mon1:this.equation) {
			result.addMonomial(new Monomial(mon1.getCoefficient()*mon1.getDegree(), mon1.getDegree()-1));
		}

		return result;
	}


	public Polynom integration(Polynom p1) {											//INTEGRATION
		Polynom result = new Polynom();													//We have the monomial cx^d. The formula for integration is:(c/(d+1))x^(d+1).
																						//Apply this for every monomial.
		for(Monomial mon1:this.equation) {
			result.addMonomial(new Monomial(mon1.getCoefficient()/(mon1.getDegree()+1), mon1.getDegree()+1));
		}
		return result;
	}

}
